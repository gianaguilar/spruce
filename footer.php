<footer>
    <div class="site-footer">
        <div class="footer-logo">
            <?php get_template_part( 'img/logo', 'name-white' ); ?>
        </div>

        <ul class="footer-contact">
            <li><b>Call us:</b> (02) 519 2937</li>
            <li><b>Write us:</b> <a href="mailto:someone@example.com">events@sprucefloraldesigns.com</a> </li>
        </ul>

        <ul class="footer-social">
            <li><a href="#"><b>IG:</b> @the.floralist</a></li>
            <li><a href="#"><b>FB:</b> Spruce Floral Designs</a></li>
        </ul>
    </div>

    <div class="site-footer-2">
        <div class="copyright">
            © <?php echo date('Y'); ?> Spruce Floral Designs
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
