jQuery(document).ready(function($){

    // ----------------------------------------
    // Accordion
    // ----------------------------------------

    $('.accordion-title').click(function(){
        $(this).toggleClass('active');
        $(this).next('.accordion-content').slideToggle(200);
    });


    // ----------------------------------------
    // Magnific Popup
    // ----------------------------------------

    $('.magnific-gallery').each(function(){
        var gallery = $(this);
        var galleryImages = $(this).data('links').split(',');

        var items = [];
        for(var i=0;i<galleryImages.length; i++){
            items.push({
                src:galleryImages[i],
                title:''
            });
        }

        gallery.magnificPopup({
            mainClass: 'mfp-fade',
            items:items,
            gallery:{
                enabled: true,
                tPrev: $(this).data('prev-text'),
                tNext: $(this).data('next-text')
            },
            type: 'image',
        });
    });


    // Menu Trigger
    $('.menu-trigger').click(function() {
        $('.top-bar-menu').toggleClass('isOpen');
        $('body').toggleClass('menu-isOpen');
    });

    $('.disable-bg').click(function() {
        $('.top-bar-menu').toggleClass('isOpen');
        $('body').toggleClass('menu-isOpen');
    });
});


// JavaScript
// window.sr = ScrollReveal();
// sr.reveal('.sr-fade', {
//     duration: 200,
//     scale: 1,
//     distance: '0px',
//     easing: 'ease-in-out',
// });