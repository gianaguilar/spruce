<?php
/*
    Template Name: Contact
*/
get_header(); ?>

<?php get_template_part('template-parts/components/banner'); ?>


<!-- Layout Map -->

    <section class="layout l-map">

        <div class="box-1">
            <input type="radio" id="radio-workshop" name="map" value="Workshop" checked/>
            <input type="radio" id="radio-admin" name="map" value="Admin"/>

            <div class="admin-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.288596776418!2d121.05943621489477!3d14.58262418142631!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c817330843df%3A0x69a89faf926a1a74!2sThe+Philippine+Stock+Exchange%2C+Inc.!5e0!3m2!1sen!2sph!4v1502953781252" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

            <div class="workshop-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3862.3675002132254!2d121.03168561489431!3d14.520959382916278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c8d55b73f931%3A0xdfb9185a09ff8f35!2s129+Diego+Silang%2C+Maynila%2C+1630+Kalakhang+Maynila!5e0!3m2!1sen!2sph!4v1502952237250" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="tabs">
                <label class="label-workshop" for="radio-workshop">Workshop</label> •
                <label class="label-admin" for="radio-admin">Admin Office</label>
            </div>
        </div>

        <div class="box-2">
            <div class="content">
                <h2 class="layout-title">Contact Us</h2>
                <p>We’d love to hear from you! If you’re inquiring about an event, the quickest way to get a quote is via email at events@sprucefloraldesings.com.</p>

                <p>
                    <b>Email Address:</b><br>
                    Events.  events@sprucefloraldesigns.com<br>Gifts Arrangements. Emily@sprucefloraldesigns.com
                </p>

                <p>
                    <b>Phone Number:</b>
                    5192937
                </p>

                <p>
                    <b>Workshop:</b><br> 129 Diego Silang Road, Phase 2, <br>AFPHOVAI (Along Bayani Road),Taguig City.
                </p>

                <p>
                    <b>Admin Office:</b><br> 1503 East Tower, Phil Stock Exchange Center, Exchange Road,<br> Ortigas Center, Pasig City
                </p>

                <p>
                    <b>Instagram:</b> <a href="https://www.instagram.com/the.floralist/">@the.floralist</a><br>
                    <b>Facebook:</b> <a href="https://www.facebook.com/Spruce-Floral-Designs-182469179835/">Spruce Floral Designs</a>
                </p>
            </div>
        </div>
    </section>

<!-- Layout 7 -->

<!-- Layout Contact Form -->

    <section class="layout l-cform">

        <div class="box-1">
            <div class="content cform">
                <h2 class="layout-title">Contact Form</h2>
                <?php echo do_shortcode('[formidable id=6]'); ?>
            </div>
        </div>

        <div class="box-2">
            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-11.jpg';?>" alt="">
        </div>

    </section>


<?php get_footer(); ?>