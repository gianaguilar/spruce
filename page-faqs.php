<?php
/*
    Template Name: FAQs
*/
get_header(); ?>

<?php get_template_part('template-parts/components/banner' ); ?>

<main class="container">
    <header class="page-header">
        <h2>Frequently Asked Questions</h2>
    </header><!-- /header -->

    <?php if ( have_rows( 'faq' ) ) : ?>
        <?php while ( have_rows( 'faq' ) ) : the_row(); ?>
            <section class="faq-topic">
                <div class="accordion-container">
                    <h2 class="faq-heading"><?php the_sub_field( 'faq_topic' ); ?></h2>

                    <div class="accordion">
                        <?php if ( have_rows( 'faq_questions' ) ) : ?>
                        <?php while ( have_rows( 'faq_questions' ) ) : the_row(); ?>
                            <div class="accordion-item">
                                <h3 class="accordion-title"><?php the_sub_field( 'question' ); ?></h3>
                                <div class="accordion-content">
                                    <?php the_sub_field( 'answer' ); ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
                </div>
            </section>
        <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>

</main>

<?php get_footer(); ?>