<?php
/*
    Template Name: Blog
*/
get_header(); ?>

<div class="banner" style="background-image: url(<?php echo esc_url( get_template_directory_uri()) . '/img/banner.jpg';?>)">
    <div class="banner-logo">
        <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/logo.svg';?>" alt="Spruce Floral Designs">
    </div>

    <h2 class="page-title">Blog</h2>
</div>


<!-- Layout 1 -->

    <section class="layout l-1">

        <div class="box-1">
            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/6a.jpg';?>" alt="Easy Elegance">
        </div>

        <div class="box-2">

            <div class="content">
                <h2 class="layout-title"><a href="<?php echo esc_url( home_url( '/easy-elegance' ) ); ?>">Easy Elegance</a></h2>
                <p>A cause for celebration and excitement, flowers play an essential role in making an affair outstanding and memorable. We work closely with our clients and take the greatest care to reflect the host’s character or represent a corporate image, while maintaining our own clean, sophisticated, design aesthetic.</p>

                <a href="<?php echo esc_url( home_url( '/easy-elegance' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>

            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-2.jpg';?>" alt="">

        </div>

    </section>

<!-- Layout 2 -->

    <section class="layout l-2">
        <div class="box-1">
            <div class="content">
                <h2 class="layout-title"><a href="<?php echo esc_url( home_url( '/modern-minimalist' ) ); ?>">Modern Minimalist</a></h2>
                <p>A venue as stylish as Blackbird needs very little styling and décor. Subtle arrangements and a neutral palette perfectly complimented the clean lines of the venue. As Coco once said “Simplicity is the key note of all true elegance.”</p>

                <a href="<?php echo esc_url( home_url( '/modern-minimalist' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>
        </div>

        <div class="boxes">
            <div class="box-2">
                <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/modern-simplicity-1-1-4.jpg">
            </div>

            <div class="box-3">
                <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/modern-simplicity-9aa-1.jpg">
            </div>
        </div>
    </section>

<!-- Layout 3 -->

    <section class="layout l-3">

        <div class="box-1">
            <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/seaside-romance-21a-1.jpg" alt="">
        </div>

        <div class="box-2">

            <div class="content">
                <h2 class="layout-title"><a href="<?php echo esc_url( home_url( '/seaside-romance' ) ); ?>">Seaside Romance</a></h2>
                <p>It would be a dream to have the same ease and elegance as this Bride. Despite the threat of rain, as well as, having the club’s golf cart breakdown on the way to the chapel, she stayed cool, calm, and collected. So Brides-to-be take note, when you keep focused on what is important – marrying the love of your life- the rest of your wedding day will be perfect, no matter what.</p>

                <a href="<?php echo esc_url( home_url( '/seaside-romance' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>

            <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/seaside-romance-14a-1.jpg" alt="">

        </div>

    </section>

<!-- Layout 4 -->

    <section class="layout l-4">

        <div class="box-1">
            <div class="content">
                <h2 class="layout-title"><a href="<?php echo esc_url( home_url( '/garden-elegance' ) ); ?>">Garden Elegance</a></h2>
                <p>Great food &amp; great service in a beautiful setting are the hallmarks of the perfect wedding. We’ve done numerous weddings at Antonio’s through the years and it has only gotten better over time.</p>
                <a href="<?php echo esc_url( home_url( '/garden-elegance' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>
        </div>

        <div class="box-2">
            <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/garden-elegance-5a-1.jpg" alt="">
        </div>

    </section>

<!-- Layout 5 -->

    <section class="layout l-5">

        <div class="box-1">
            <div>
                <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/bold-bright-2a-1.png" alt="">
            </div>
            <div>
                <img src="http://www.sprucefloraldesigns.com/wp-content/uploads/2017/09/bold-bright-7a-1.png" alt="">
            </div>
        </div>

        <div class="box-2">
            <div class="content">
                <h2 class="layout-title"><a href="<?php echo esc_url( home_url( '/a-bold-bright-island-wedding' ) ); ?>">A Bold & Bright Island Wedding</a></h2>
                <p>After a quick glance at this wedding, how can you not fall in love with the gorgeous, bright colors wrapped into this couple’s big day. From the vibrant bridal bouquet to the romantic seaside venue, the bold use of reds, oranges and yellow makes their wedding a sight to see.</p>
                <a href="<?php echo esc_url( home_url( '/a-bold-bright-island-wedding' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>
        </div>

    </section>


<?php get_footer(); ?>