<?php get_header(); ?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article class="blog-article">
    <div class="blog-content">
        <div class="sticky">
            <!-- post -->
                <header class="blog-header">
                    <h2 class="blog-title"><?php the_title(); ?></h2>
                    <div class="post-meta"><?php the_category(', '); ?> | <?php the_time('F j, Y');?></div>
                </header><!-- /header -->
                <div>
                    <?php the_content (); ?>
                </div>

                <div class="fade"></div>
        </div>
    </div>

    <div class="blog-photos">
        <?php the_field( 'photos' ); ?>
    </div>
</article>
<?php endwhile; ?>
<!-- post navigation -->
<?php else: ?>
<!-- no posts found -->
<?php endif; ?>

<?php get_footer(); ?>