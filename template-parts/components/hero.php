<div class="hero" style="background-image: url(<?php echo esc_url( get_template_directory_uri()) . '/img/hero.jpg';?>)">
    <div class="hero-logo">
        <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/logo.svg';?>" alt="Spruce Floral Designs">
    </div>
</div>