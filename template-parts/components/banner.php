<div class="banner" style="background-image: url(<?php echo esc_url( get_template_directory_uri()) . '/img/banner.jpg';?>)">
    <div class="banner-logo">
        <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/logo.svg';?>" alt="Spruce Floral Designs">
    </div>

    <?php the_title( '<h2 class="page-title">', '</h2>' ); ?>
</div>