<div class="top-bar">
    <div class="top-bar-logo">
        <button class="menu-trigger">
            <div class="line-1"></div>
            <div class="line-2"></div>
            <div class="line-3"></div>
        </button>
        <a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <?php get_template_part('img/logo', 'name-black'); ?>
        </a>
    </div>
    <nav class="top-bar-menu">
        <ul>
            <li><a href="<?php echo esc_url( home_url( '/about' ) ); ?>">About</a></li>
            <li><a href="<?php echo esc_url( home_url( '/events' ) ); ?>">Events</a></li>
            <li><a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">Blog</a></li>
            <li><a href="<?php echo esc_url( home_url( '/faqs' ) ); ?>">FAQs</a></li>
            <li><a href="<?php echo esc_url( home_url( '/contact' ) ); ?>">Contact</a></li>
        </ul>
    </nav>
    <div class="top-bar-social">
        <ul>
            <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29"><title>Logo: Facebook</title><path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z"/></svg></a></li>
            <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><title>Logo: Instagram</title><path d="M4.066.636h19.867c1.887 0 3.43 1.543 3.43 3.43v19.868c0 1.888-1.543 3.43-3.43 3.43H4.066c-1.887 0-3.43-1.542-3.43-3.43V4.066c0-1.887 1.544-3.43 3.43-3.43zm16.04 2.97c-.66 0-1.203.54-1.203 1.202v2.88c0 .662.542 1.203 1.204 1.203h3.02c.663 0 1.204-.54 1.204-1.202v-2.88c0-.662-.54-1.203-1.202-1.203h-3.02zm4.238 8.333H21.99c.224.726.344 1.495.344 2.292 0 4.446-3.72 8.05-8.308 8.05s-8.31-3.604-8.31-8.05c0-.797.122-1.566.344-2.293H3.606v11.29c0 .584.48 1.06 1.062 1.06H23.28c.585 0 1.062-.477 1.062-1.06V11.94h.002zm-10.32-3.2c-2.963 0-5.367 2.33-5.367 5.202 0 2.873 2.404 5.202 5.368 5.202 2.965 0 5.368-2.33 5.368-5.202s-2.403-5.2-5.368-5.2z"/></svg></a></li>
        </ul>
    </div>
</div>

<div class="disable-bg"></div>