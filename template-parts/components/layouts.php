
<!-- Layout 1 -->

    <section class="layout l-1">

        <div class="box-1">
            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/6a.jpg';?>" alt="">
        </div>

        <div class="box-2">

            <div class="content">
                <h2 class="layout-title">Events</h2>
                <p>A cause for celebration and excitement, flowers play an essential role in making an affair outstanding and memorable. We work closely with our clients and take the greatest care to reflect the host’s character or represent a corporate image, while maintaining our own clean, sophisticated, design aesthetic.</p>

                <a href="<?php echo esc_url( home_url( '/events' ) ); ?>" class="layout-btn">→ View</a>
            </div>

            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-2.jpg';?>" alt="">

        </div>

    </section>

<!-- Layout 2 -->

    <section class="layout l-2">
        <div class="box-1">
            <div class="content">
                <h2 class="layout-title">About Us</h2>
                <p>Our mission is to create beautiful environments that reflect the client’s character. We realize that each event is as unique as the client, and would like to take the time out to get to know you.  We will work to develop your vision to create an elegant and sophisticated event.</p>

                <a href="<?php echo esc_url( home_url( '/about' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>
        </div>

        <div class="boxes">
            <div class="box-2">
                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-3.jpg';?>" alt="">
            </div>

            <div class="box-3">
                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-4.jpg';?>" alt="">
            </div>
        </div>
    </section>

<!-- Layout 3 -->

    <section class="layout l-3">

        <div class="box-1">
            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-5.jpg';?>" alt="">
        </div>

        <div class="box-2">

            <div class="content">
                <h2 class="layout-title">Featured Editorial</h2>
                <p>At Spruce, we believe that elegance is beauty that exhibits simplicity, consistency of design, and restraint.</p>

                <a href="<?php echo esc_url( home_url( '/easy-elegance' ) ); ?>" class="layout-btn">→ Read More</a>
            </div>

            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/img-6.jpg';?>" alt="">

        </div>

    </section>

