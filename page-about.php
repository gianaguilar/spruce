<?php
/*
    Template Name: About
*/
get_header(); ?>

<?php get_template_part('template-parts/components/banner' ); ?>

<!-- Layout 4 -->

    <section class="layout l-4">

        <div class="box-1">
            <div class="content">
                <h2 class="layout-title">About Us</h2>
                <p>The Spruce Floral Design Team works closely with each client to create an affair that is personally tailored.  Our mission is to create beautiful environments that reflect the client’s character. We realize that each event is as unique as the client.  We will work to develop your vision to create an elegant and sophisticated event.</p>

                <p>We advise our clients on both the overall look as well as the budget recommendations.  In the past we have designed decorations for diverse locations, from luxurious lanais and gardens to large events spaces.</p>

                <p>Spruce Floral Designs has a hard-working and dedicated team that keeps themselves updated on trends and developments in the industry by regularly attending workshops locally and abroad. The team works closely with clients and takes the greatest care to reflect the host’s character, while maintaining Spruce’s clean, simple aesthetic.</p>
            </div>
        </div>

        <div class="box-2">
            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/about.jpg';?>" alt="">
        </div>

    </section>

<!-- Layout 5 -->

    <section class="layout l-5">

        <div class="box-1">
            <div>
                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/destination-1.jpg';?>" alt="">
            </div>
            <div>
                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/destination-2.jpg';?>" alt="">
            </div>
        </div>

        <div class="box-2">
            <div class="content">
                <h2 class="layout-title">Local Destination Weddings</h2>
                <p>We love the challenge of putting together a beautiful event in a unique location. Whether it be a family home, provincial retreat, or island paradise we take care of planning out both the design and logistics of your celebration.   We’ve worked with couples bring their favorite destinations to life in Bacolod, Balesin, Batangas, Boracay, Cebu, and Tagaytay.</p>
            </div>
        </div>

    </section>

<!-- Layout 6 -->

    <section class="layout l-6">

        <div class="box-1">
            <div class="content">
                <h2 class="layout-title">Press / Clients</h2>
                <p>We have had the privilege of working with a number of corporations and individuals throughout the years; namely,</p>

                <p>AVEDA. BULGARI PERFUMES. BURTS BEES. CASA BELLA. CITEM. HARLAN & HOLDEN. JOHNNY WALKER BLACK. LOUIS VUITTON. MAYBANK. PANDORA. PORCELONOSA. RIMADESIO. ROCKWELL LAND. HSBC PRIVATE BANKING.</p>

                <p>We have been featured by…<br>
                BRIDE & BREAKFAST. ELLE DÉCOR.  MARTHA STEWART WEDDINGS PHILIPPINES.  MANILA BULLETIN. METRO HOME.  METRO SOCIERY.  METRO WEDDINGS.  PHILIPPINE STAR. TOWN & COUNTRY MAGAZINE.  WEDDING ESSENTIALS MAGAZINE.
                </p>
            </div>
        </div>

        <div class="box-2">
            <div>
                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/press.jpg';?>" alt="">
            </div>
        </div>

    </section>

<?php get_footer(); ?>