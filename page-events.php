<?php
/*
    Template Name: Events
*/
get_header(); ?>

    <?php get_template_part('template-parts/components/banner'); ?>

    <div class="masonry-container">
        <?php the_field( 'infinite_scroll' ); ?>
    </div>
<?php get_footer(); ?>