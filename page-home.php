<?php
/*
    Template Name: Home
*/
get_header(); ?>

    <?php get_template_part('template-parts/components/hero'); ?>

    <?php get_template_part('template-parts/components/layouts') ?>

<?php get_footer(); ?>